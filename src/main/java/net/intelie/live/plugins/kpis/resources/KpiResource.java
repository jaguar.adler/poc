package net.intelie.live.plugins.kpis.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * KpiResource interface
 */
@Path("/kpi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@SuppressWarnings("JavaDoc")
public interface KpiResource {

    /**
     * Retrieve a collection with all KPIs for a given {@code name}.
     *
     * @param name The KPI name.
     * @return A collection with all KPIs for the given {@code name}.
     * @returnWrapped java.util.Collection
     * @RequestHeader X-Auth-Token The authentication token.
     * @HTTP 200 Request made successfully.
     * @HTTP 204 Request made successfully but return an empty collection.
     * @HTTP 400 A request was made to this resource with invalid parameters.
     * @HTTP 404 The requested resource does not exist.
     */
    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    Response list(@PathParam("name") String name);

    /**
     * Retrieve a single KPI for a given {@code name} with the given {@code id}.
     *
     * @param name The KPI name.
     * @return A collection with all KPIs for the given {@code name}.
     * @RequestHeader X-Auth-Token The authentication token.
     * @ResponseHeader Location The location of the created KPI.
     * @requestExample application/json { "name" : "slip-to-slip", "abbr" : "s2s" }
     * @HTTP 204 Request made successfully, but has no body.
     * @HTTP 400 A request was made to this resource with invalid parameters.
     */
    @POST
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    Response create(@PathParam("name") String name, Object kpi);
}



